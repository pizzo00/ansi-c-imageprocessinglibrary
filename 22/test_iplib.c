#include "ip_lib.h"
#include <math.h>
#include <stdio.h>
#include <assert.h>

#define ui unsigned int
#define FLOAT_EQ_PRECISION 0.0001

/**** Test Methods ****/
int totalTests = 0;
int failedTests = 0;

void printSummary()
{
    int passedTests = totalTests - failedTests;
    printf("Tests Passed %d/%d\n", passedTests, totalTests);
}

void test(int test, const char* description)
{
    totalTests++;
    if(!test)
    {
        printf("%s - FAILED\n", description);
        failedTests++;
    }
}

int eq_float(float a, float b) { return fabs(a - b) < FLOAT_EQ_PRECISION; }

int check_mat_equal(ip_mat *a, ip_mat* b)
{
    ui iH, iW, iK;
    if (a->h != b->h) return 0;
    if (a->w != b->w) return 0;
    if (a->k != b->k) return 0;

    for(iH = 0; iH < a->h; iH++)
    {
        for(iW = 0; iW < a->w; iW++)
        {
            for(iK = 0; iK < a->k; iK++)
            {
                if(!eq_float(get_val(a, iH, iW, iK), get_val(b, iH, iW, iK)))
                    return 0;
            }
        }
    }
    return 1;
}
int check_mat_equal_to_array(ip_mat *a, float* b[], ui bH, ui bW, ui bK)
{
    ui iH, iW, iK;
    if (a->h != bH) return 0;
    if (a->w != bW) return 0;
    if (a->k != bK) return 0;

    for(iH = 0; iH < bH; iH++)
    {
        for(iW = 0; iW < bW; iW++)
        {
            for(iK = 0; iK < bK; iK++)
            {
                if(!eq_float(get_val(a, iH, iW, iK), b[iK][(iH * bW) + iW]))
                    return 0;
            }
        }
    }
    return 1;
}
int check_mat_equal_to_array_single_channel(ip_mat *a, float* b, ui bH, ui bW)
{
    float *newB[1];
    newB[0] = b;
    return check_mat_equal_to_array(a, newB, bH, bW, 1);
}

ip_mat* create_mat_from_array(float* a[], ui h, ui w, ui k)
{
    ip_mat* out = ip_mat_create(h, w, k, 0.);
    ui iH, iW, iK;

    for(iH = 0; iH < h; iH++)
    {
        for(iW = 0; iW < w; iW++)
        {
            for(iK = 0; iK < k; iK++)
            {
                set_val(out, iH, iW, iK, a[iK][(iH * w) + iW]);
            }
        }
    }
    return out;
}
ip_mat* create_mat_from_array_single_channel(float* a, ui h, ui w)
{
    float *newA[1];
    newA[0] = a;
    return create_mat_from_array(newA, h, w, 1);
}

void init_array(float* in, ui l, float v)
{
    ui i;
    for(i = 0; i < l; i++)
    {
        in[i] = v;
    }
}

/****************/

/**** PART 1 ****/

/* Check ip_mat_create and ip_mat_free */
void c_ip_mat_create_free()
{
    ip_mat *a;
    float *b[3];
    float b0[50];
    float b1[50];
    float b2[50];
    b[0] = b0;
    b[1] = b1;
    b[2] = b2;

    init_array(b0, 50, -10.);
    init_array(b1, 50, -10.);
    init_array(b2, 50, -10.);
    a = ip_mat_create(10, 5, 3, -10.);
    test(check_mat_equal_to_array(a, b, 10, 5, 3), "ip_mat_create h=10 w=5 k=3 v=-10.");
    ip_mat_free(a);

    /* Create a mat with 0 lenght */
    a = ip_mat_create(0, 0, 0, 0.);
    ip_mat_free(a);

    a = ip_mat_create(3, 0, 0, 0.);
    ip_mat_free(a);

    a = ip_mat_create(3, 3, 0, 0.);
    ip_mat_free(a);

    a = ip_mat_create(3, 3, 3, 0.);
    ip_mat_free(a);

    /* Should not crash, just do nothing */
    ip_mat_free(NULL);
}

void c_compute_stats()
{
    ip_mat *a;
    float val;
    float b[6] =
    {
        -1., -1., -1.,
        -3., -3., -3.,
    };

    /* All items equal */
    val = 0.;
    a = ip_mat_create(10, 5, 3, val);
    compute_stats(a);
    test(eq_float(a->stat[0].min, val), "compute_stats (1 - ch0 - min) ip_mat v=0.");
    test(eq_float(a->stat[0].max, val), "compute_stats (1 - ch0 - max) ip_mat v=0.");
    test(eq_float(a->stat[0].mean, val), "compute_stats (1 - ch0 - mean) ip_mat v=0.");
    test(eq_float(a->stat[1].min, val), "compute_stats (1 - ch1 - min) ip_mat v=0.");
    test(eq_float(a->stat[1].max, val), "compute_stats (1 - ch1 - max) ip_mat v=0.");
    test(eq_float(a->stat[1].mean, val), "compute_stats (1 - ch1 - mean) ip_mat v=0.");
    test(eq_float(a->stat[2].min, val), "compute_stats (1 - ch2 - min) ip_mat v=0.");
    test(eq_float(a->stat[2].max,  val), "compute_stats (1 - ch2 - max) ip_mat v=0.");
    test(eq_float(a->stat[2].mean, val), "compute_stats (1 - ch2 - mean) ip_mat v=0.");
    ip_mat_free(a);

    val = -158.9;
    a = ip_mat_create(10, 5, 3, val);
    compute_stats(a);
    test(eq_float(a->stat[0].min, val), "compute_stats (2 - ch0 - min) ip_mat v=-158.9");
    test(eq_float(a->stat[0].max, val), "compute_stats (2 - ch0 - max) ip_mat v=-158.9");
    test(eq_float(a->stat[0].mean, val), "compute_stats (1 - ch0 - mean) ip_mat v=-158.9");
    test(eq_float(a->stat[1].min, val), "compute_stats (2 - ch1 - min) ip_mat v=-158.9");
    test(eq_float(a->stat[1].max, val), "compute_stats (2 - ch1 - max) ip_mat v=-158.9");
    test(eq_float(a->stat[1].mean, val), "compute_stats (1 - ch1 - mean) ip_mat v=-158.9");
    test(eq_float(a->stat[2].min, val), "compute_stats (2 - ch2 - min) ip_mat v=-158.9");
    test(eq_float(a->stat[2].max, val), "compute_stats (2 - ch2 - max) ip_mat v=-158.9");
    test(eq_float(a->stat[2].mean, val), "compute_stats (1 - ch2 - mean) ip_mat v=-158.9");
    ip_mat_free(a);


    a = create_mat_from_array_single_channel(b, 3, 2);
    compute_stats(a);
    test(a->stat[0].min == -3.f, "compute_stats (3 - ch0 - min)");
    test(a->stat[0].max == -1.f, "compute_stats (3 - ch0 - max)");
    test(floor(a->stat[0].mean) == floor(-2.f), "compute_stats (3 - ch0 - mean)");
    ip_mat_free(a);

}

void c_ip_mat_copy()
{
    ip_mat *a, *b;

    a = ip_mat_create(100, 70, 8, -15.);
    b = ip_mat_copy(a);
    test(check_mat_equal(a, b), "ip_mat_copy (1) w=100 h=7 k=5 v=-15.");
    ip_mat_free(a);
    ip_mat_free(b);

    a = ip_mat_create(10, 50, 2, 5.);
    set_val(a, 0, 0, 0, -1.12);
    set_val(a, 0, 0, 1, -1.23);
    set_val(a, 0, 49, 0, -1.34);
    set_val(a, 9, 0, 0, -1.45);
    set_val(a, 9, 49, 1, -1.56);
    b = ip_mat_copy(a);
    test(check_mat_equal(a, b), "ip_mat_copy (2) w=10 h=50 k=2");
    ip_mat_free(a);
    ip_mat_free(b);
}

void c_ip_mat_subset()
{
    ip_mat *a, *b;
    float c[12] =
    {
        1., 2., 3.,
        4., 5., 6.,
        10., 20., 30.,
        40., 50., 60.,
    };
    float d[1] =
    {
        60.
    };
    float e[3] =
    {
        5.,
        20.,
        50.,
    };

    a = ip_mat_create(100, 70, 8, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_subset(a, 0, 100, 0, 70);
    test(check_mat_equal(a, b), "ip_mat_subset (1) init_random / subset on all ip_mat");
    ip_mat_free(a);
    ip_mat_free(b);

    a = ip_mat_create(100, 70, 8, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_subset(a, 0, 0, 0, 0);
    test((b->h == 0 && b->w == 0), "ip_mat_subset (2) init_random / subset [0, 0][0, 0]");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(c, 4, 3);
    b = ip_mat_subset(a, 3, 4, 2, 3);
    test(check_mat_equal_to_array_single_channel(b, d, 1, 1), "ip_mat_subset (3) subset to one cell");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(c, 4, 3);
    b = ip_mat_subset(a, 1, 4, 1, 2);
    test(check_mat_equal_to_array_single_channel(b, e, 3, 1), "ip_mat_subset (4) subset to 3x1");
    ip_mat_free(a);
    ip_mat_free(b);
}

void c_ip_mat_concat()
{
    ip_mat *a, *b, *c;
    float aa[12] =
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float ab[4] =
    {
        4.,
        5.,
        6.,
        7.,
    };
    float ar[16] =
    {
        1., 2., 3., 4.,
        1., 2., 3., 5.,
        1., 2., 3., 6.,
        1., 2., 3., 7.,
    };


    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);

    b = ip_mat_create(0, 70, 3, 0.);
    c = ip_mat_concat(a, b, 0);
    test(check_mat_equal(a, c), "ip_mat_concat (1) concat init_random with void dim=0");
    ip_mat_free(c);
    ip_mat_free(b);

    b = ip_mat_create(100, 0, 3, 0.);
    c = ip_mat_concat(a, b, 1);
    test(check_mat_equal(a, c), "ip_mat_concat (2) concat init_random with void dim=1");
    ip_mat_free(c);
    ip_mat_free(b);

    b = ip_mat_create(100, 70, 0, 0.);
    c = ip_mat_concat(a, b, 2);
    test(check_mat_equal(a, c), "ip_mat_concat (3) concat init_random with void dim=2");
    ip_mat_free(c);
    ip_mat_free(b);

    ip_mat_free(a);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = create_mat_from_array_single_channel(ab, 4, 1);
    c = ip_mat_concat(a, b, 1);
    test(check_mat_equal_to_array_single_channel(c, ar, 4, 4), "ip_mat_concat (4) concat manually defined dim=1");
    ip_mat_free(c);
    ip_mat_free(b);
    ip_mat_free(a);

}

void c_ip_mat_sum()
{
    ip_mat *a, *b, *c;
    float aa[12] =
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float ab[12] =
    {
        -3., -2., -1.,
        -2., -1.,  0.,
        -1.,  0.,  1.,
         0.,  1.,  2.,
    };
    float ar[12] =
    {
        -2., 0., 2.,
        -1., 1., 3.,
         0., 2., 4.,
         1., 3., 5.,
    };

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_create(100, 70, 3, 0.);
    c = ip_mat_sum(a, b);
    test(check_mat_equal(a, c), "ip_mat_sum (1) Sum 0");
    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(c);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = create_mat_from_array_single_channel(ab, 4, 3);
    c = ip_mat_sum(a, b);
    test(check_mat_equal_to_array_single_channel(c, ar, 4, 3), "ip_mat_sum (2) sum manually defined");
    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(c);
}

void c_ip_mat_sub()
{
    ip_mat *a, *b, *c;
    float aa[12] =
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float ab[12] =
    {
        3.,  2.,  1.,
        2.,  1.,  0.,
        1.,  0., -1.,
        0., -1., -2.,
    };
    float ar[12] =
    {
        -2., 0., 2.,
        -1., 1., 3.,
        0., 2., 4.,
        1., 3., 5.,
    };

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_create(100, 70, 3, 0.);
    c = ip_mat_sub(a, b);
    test(check_mat_equal(a, c), "ip_mat_sub (1) Sub 0");
    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(c);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = create_mat_from_array_single_channel(ab, 4, 3);
    c = ip_mat_sub(a, b);
    test(check_mat_equal_to_array_single_channel(c, ar, 4, 3), "ip_mat_sub (2) sub manually defined");
    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(c);
}

void c_ip_mat_mul_scalar()
{
    ip_mat *a, *b;
    float aa[12] =
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float ar[12] =
    {
         -0.5, -1., -1.5,
         -0.5, -1., -1.5,
         -0.5, -1., -1.5,
         -0.5, -1., -1.5,
    };

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_mul_scalar(a, 1.);
    test(check_mat_equal(a, b), "ip_mul_scalar (1) mul 1");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = ip_mat_mul_scalar(a, -0.5);
    test(check_mat_equal_to_array_single_channel(b, ar, 4, 3), "ip_mul_scalar (2) mul manually defined *0.5");
    ip_mat_free(a);
    ip_mat_free(b);
}

void c_ip_mat_add_scalar()
{
    ip_mat *a, *b;
    float aa[12] =
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float ar[12] =
    {
        0, 1., 2,
        0, 1., 2,
        0, 1., 2,
        0, 1., 2,
    };

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_add_scalar(a, 0.);
    test(check_mat_equal(a, b), "ip_add_scalar (1) add 0");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = ip_mat_add_scalar(a, -1);
    test(check_mat_equal_to_array_single_channel(b, ar, 4, 3), "ip_add_scalar (2) add manually defined -1");
    ip_mat_free(a);
    ip_mat_free(b);
}

void c_ip_mat_mean()
{
    ip_mat *a, *b, *c;
    float aa[12] =
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float ab[12] =
    {
        0., 1., 2.,
        1., 2., 3.,
        2., 3., 4.,
        3., 4., 5.,
    };
    float ar[12] =
    {
        0.5, 1.5, 2.5,
        1.0, 2.0, 3.0,
        1.5, 2.5, 3.5,
        2.0, 3.0, 4.0,
    };

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_mean(a, a);
    test(check_mat_equal(a, b), "ip_mat_mean (1) mean on the same val");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = create_mat_from_array_single_channel(ab, 4, 3);
    c = ip_mat_mean(a, b);
    test(check_mat_equal_to_array_single_channel(c, ar, 4, 3), "ip_mat_mean (2) mean manually defined");
    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(c);
}

/****************/

/**** PART 2 ****/

void c_ip_mat_to_gray_scale()
{
    ip_mat *a, *b;
    float *aa[2];
    float *ar[2];
    float aa0[9]=
    {
        1., 2., 3.,
        1., 2., 3.,
        1., 2., 3.,
    };
    float aa1[9]=
    {
        1., 2., 3.,
        0., 0., 0.,
        3., 4., 5.,
    };
    float arCh[9] =
    {
        1.0, 2.0, 3.0,
        0.5, 1.0, 1.5,
        2.0, 3.0, 4.0,
    };
    aa[0] = aa0;
    aa[1] = aa1;
    ar[0] = arCh;
    ar[1] = arCh;

    a = ip_mat_create(100, 70, 3, -15.5);
    b = ip_mat_to_gray_scale(a);
    test(check_mat_equal(a, b), "ip_mat_to_gray_scale (1) all val are -15.5");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array(aa, 3, 3, 2);
    b = ip_mat_to_gray_scale(a);
    test(check_mat_equal_to_array(b, ar, 3, 3, 2), "ip_mat_to_gray_scale (2) manually defined");
    ip_mat_free(a);
    ip_mat_free(b);
}

void c_ip_mat_blend()
{
    ip_mat *a, *b, *c;

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(b, 0, 100);

    c = ip_mat_blend(a, b, 0.);
    test(check_mat_equal(c, b), "ip_mat_blend (1) alpha=0.");
    ip_mat_free(c);

    c = ip_mat_blend(a, b, 1.);
    test(check_mat_equal(c, a), "ip_mat_blend (2) alpha=1.");
    ip_mat_free(c);

    c = ip_mat_blend(a, a, 0.2);
    test(check_mat_equal(c, a), "ip_mat_blend (3) Same ip_mat alpha=0.2");
    ip_mat_free(c);

    c = ip_mat_blend(a, a, 0.8);
    test(check_mat_equal(c, a), "ip_mat_blend (4) Same ip_mat alpha=0.8");
    ip_mat_free(c);

    ip_mat_free(a);
    ip_mat_free(b);


}

void c_ip_mat_brighten()
{
    ip_mat *a, *b;
    float aa[12] =
    {
        0., 1., 2.,
        0., 1., 2.,
        0., 1., 2.,
        0., 1., 2.,
    };
    float ar[12] =
    {
        -1., 0., 1.,
        -1., 0., 1.,
        -1., 0., 1.,
        -1., 0., 1.,
    };

    a = ip_mat_create(100, 70, 3, 0.);
    ip_mat_init_random(a, 0, 100);
    b = ip_mat_add_scalar(a, 0.);
    test(check_mat_equal(a, b), "ip_mat_brighten (1) add 0");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(aa, 4, 3);
    b = ip_mat_add_scalar(a, -1);
    test(check_mat_equal_to_array_single_channel(b, ar, 4, 3), "ip_mat_brighten (2) add manually defined -1");
    ip_mat_free(a);
    ip_mat_free(b);
}

/****************/

/**** PART 3 ****/

void c_ip_mat_convolve()
{
    ip_mat *a, *f, *r, *s;
    float c[9] =
    {
        1., 0., -1.,
        1., 0., -1.,
        1., 0., -1.,
    };
    float d[25] =
    {
        7., 2., 3., 3., 8.,
        4., 5., 3., 8., 4.,
        3., 3., 2., 8., 4.,
        2., 8., 7., 2., 7.,
        5., 4., 4., 5., 4.,
    };
    float e[9] =
    {
         6., -9., -8.,
        -3., -2., -3.,
        -3.,  0., -2.,
    };

    a = create_mat_from_array_single_channel(d, 5, 5);
    f = create_mat_from_array_single_channel(c, 3, 3);
    r = ip_mat_convolve(a, f);
    s = ip_mat_subset(r, 1, 4, 1, 4); /* Matrix return a 3x3 as in the pdf specifications */
    test(check_mat_equal_to_array_single_channel(s, e, 3, 3), "c_ip_mat_convolve (1) specifications example");
    ip_mat_free(a);
    ip_mat_free(f);
    ip_mat_free(r);
    ip_mat_free(s);
}

void c_ip_mat_padding()
{
    ip_mat *a, *b;
    float c[12] =
    {
        1., 2., 3.,
        4., 5., 6.,
        10., 20., 30.,
        40., 50., 60.,
    };
    float d[30] =
    {
        0., 0., 0.,
        0., 0., 0.,
        0., 0., 0.,
        1., 2., 3.,
        4., 5., 6.,
        10., 20., 30.,
        40., 50., 60.,
        0., 0., 0.,
        0., 0., 0.,
        0., 0., 0.,
    };
    float e[20] =
    {
        0.,  1.,  2.,  3., 0.,
        0.,  4.,  5.,  6., 0.,
        0., 10., 20., 30., 0.,
        0., 40., 50., 60., 0.,
    };

    a = create_mat_from_array_single_channel(c, 4, 3);
    b = ip_mat_padding(a, 0, 0);
    test(check_mat_equal(a, b), "ip_mat_padding (1) pad_h: 0 pad_w: 0");
    ip_mat_free(a);
    ip_mat_free(b);

    a = ip_mat_create(0, 0, 5, 0.);
    b = ip_mat_padding(a, 1, 1);
    ip_mat_free(a);
    a = ip_mat_create(2, 2, 5, 0.);
    test(check_mat_equal(a, b), "ip_mat_padding (2) pad_h: 0 pad_w: 0 - a(0,0)");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(c, 4, 3);
    b = ip_mat_padding(a, 3, 0);
    test(check_mat_equal_to_array_single_channel(b, d, 10, 3), "ip_mat_padding (3) pad_h: 3 pad_w: 0");
    ip_mat_free(a);
    ip_mat_free(b);

    a = create_mat_from_array_single_channel(c, 4, 3);
    b = ip_mat_padding(a, 0, 1);
    test(check_mat_equal_to_array_single_channel(b, e, 4, 5), "ip_mat_padding (4) pad_h: 0 pad_w: 1");
    ip_mat_free(a);
    ip_mat_free(b);
}

void c_create_sharpen_filter()
{
    ip_mat *a;
    float f[9] =
    {
         0., -1.,  0.,
        -1.,  5., -1.,
         0., -1.,  0.
    };

    a = create_sharpen_filter();
    test(check_mat_equal_to_array_single_channel(a, f, 3, 3), "create_sharpen_filter");
    ip_mat_free(a);
}

void c_create_edge_filter()
{
    ip_mat *a;
    float f[9] =
    {
        -1., -1., -1.,
        -1.,  8., -1.,
        -1., -1., -1.,
    };

    a = create_edge_filter();
    test(check_mat_equal_to_array_single_channel(a, f, 3, 3), "create_edge_filter");
    ip_mat_free(a);
}

void c_create_emboss_filter()
{
    ip_mat *a;

    float f[9] =
    {
        -2., -1.,  0.,
        -1.,  1.,  1.,
         0.,  1.,  2.
    };

    a = create_emboss_filter();
    test(check_mat_equal_to_array_single_channel(a, f, 3, 3), "create_emboss_filter");
    ip_mat_free(a);
}

void c_create_average_filter()
{
    ip_mat *a;
    float *f3_1_5[5];
    float f3_1[3];

    float f1[1], f3[9], f5[25], f7[49];
    init_array(f1,  1, 1./1.);
    init_array(f3,  9, 1./9.);
    init_array(f5, 25, 1./25.);
    init_array(f7, 49, 1./49.);

    a = create_average_filter(1, 1, 1);
    test(check_mat_equal_to_array_single_channel(a, f1, 1, 1), "create_average_filter (1, 1, 1)");
    ip_mat_free(a);

    a = create_average_filter(3, 3, 1);
    test(check_mat_equal_to_array_single_channel(a, f3, 3, 3), "create_average_filter (3, 3, 1)");
    ip_mat_free(a);

    a = create_average_filter(5, 5, 1);
    test(check_mat_equal_to_array_single_channel(a, f5, 5, 5), "create_average_filter (5, 5, 1)");
    ip_mat_free(a);

    a = create_average_filter(7, 7, 1);
    test(check_mat_equal_to_array_single_channel(a, f7, 7, 7), "create_average_filter (7, 7, 1)");
    ip_mat_free(a);

    /* *f3_1_5[5] */
    init_array(f3_1,  3, 1./3.);
    f3_1_5[0] = f3_1;
    f3_1_5[1] = f3_1;
    f3_1_5[2] = f3_1;
    f3_1_5[3] = f3_1;
    f3_1_5[4] = f3_1;

    a = create_average_filter(3, 1, 5);
    test(check_mat_equal_to_array(a, f3_1_5, 3, 1, 5), "create_average_filter (3, 1, 5)");
    ip_mat_free(a);
}

void c_clamp()
{
    ip_mat *a;
    float min, max;

    a = ip_mat_create(10, 70, 5, 0.);

    min = 0;
    max = 255;
    ip_mat_init_random(a, 0., 100.);
    clamp(a, min, max);
    compute_stats(a);
    test((a->stat[0].min >= min && a->stat[0].max <= max), "clamp (0 - ch0) [0, 255]");
    test((a->stat[1].min >= min && a->stat[1].max <= max), "clamp (0 - ch1) [0, 255]");
    test((a->stat[2].min >= min && a->stat[2].max <= max), "clamp (0 - ch2) [0, 255]");
    test((a->stat[3].min >= min && a->stat[3].max <= max), "clamp (0 - ch3) [0, 255]");
    test((a->stat[4].min >= min && a->stat[4].max <= max), "clamp (0 - ch4) [0, 255]");

    min = -50;
    max = -1;
    ip_mat_init_random(a, 0., 100.);
    clamp(a, min, max);
    compute_stats(a);
    test((a->stat[0].min >= min && a->stat[0].max <= max), "clamp (1 - ch0) [-50, -1]");
    test((a->stat[1].min >= min && a->stat[1].max <= max), "clamp (1 - ch1) [-50, -1]");
    test((a->stat[2].min >= min && a->stat[2].max <= max), "clamp (1 - ch2) [-50, -1]");
    test((a->stat[3].min >= min && a->stat[3].max <= max), "clamp (1 - ch3) [-50, -1]");
    test((a->stat[4].min >= min && a->stat[4].max <= max), "clamp (1 - ch4) [-50, -1]");

    min = -1;
    max = -1;
    ip_mat_init_random(a, 0., 100.);
    clamp(a, min, max);
    compute_stats(a);
    test((a->stat[0].min >= min && a->stat[0].max <= max), "clamp (2 - ch0) [-1, -1]");
    test((a->stat[1].min >= min && a->stat[1].max <= max), "clamp (2 - ch1) [-1, -1]");
    test((a->stat[2].min >= min && a->stat[2].max <= max), "clamp (2 - ch2) [-1, -1]");
    test((a->stat[3].min >= min && a->stat[3].max <= max), "clamp (2 - ch3) [-1, -1]");
    test((a->stat[4].min >= min && a->stat[4].max <= max), "clamp (2 - ch4) [-1, -1]");

    ip_mat_free(a);
}

/****************/

int main()
{
    /* Part 1 */
    c_ip_mat_create_free();
    c_compute_stats();
    c_ip_mat_copy();
    c_ip_mat_subset();
    c_ip_mat_concat();

    c_ip_mat_sum();
    c_ip_mat_sub();
    c_ip_mat_mul_scalar();
    c_ip_mat_add_scalar();
    c_ip_mat_mean();

    /* Part 2 */
    c_ip_mat_to_gray_scale();
    c_ip_mat_blend();
    c_ip_mat_brighten();

    /* Part 3 */
    c_ip_mat_convolve();
    c_ip_mat_padding();

    c_create_sharpen_filter();
    c_create_edge_filter();
    c_create_emboss_filter();
    c_create_average_filter();

    c_clamp();


    printSummary();
    return 0;
}
