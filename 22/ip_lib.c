/*
 * TimeLimitExceeded - Group 22
 * Davide Pizzolato - 881347
 */

/*
 Created by Sebastiano Vascon on 23/03/20
 Edited by Pizzolato Davide 881347
*/

#include <stdio.h>
#include "ip_lib.h"
#include "bmp.h"

#define ui unsigned int

/* Put the MY_ prefix because MAX and MIN can be defined in other library */
#define MY_MAX(a, b) ((a) > (b) ? (a) : (b))
#define MY_MIN(a, b) ((a) < (b) ? (a) : (b))

/* Workaround for unused parameter warning */
#define UNUSED(x) (void)(x)

/* If true enable convolve with 1ch filter and multi channel image */
#define SINGLE_CHANNEL_CONVOLVE 1

/* TODO:
 * (DONE) Write formal comment for my private method
 * (DONE) Unit Testing
 * (DONE) Check Convolve k
 */

/**** MAIN PRIVATE FUNCTIONS ****/

/**
 * Used instead of assert in order to close the process with code 1
 * @param condition If false close the process
 */
void exit_if_not(int condition)
{
    if(!condition) exit(1);
}

/**
 * Use this type to declare an operator that take one value and an array of parameters
 *  @param a The input value
 *  @param args An array of parameters
 *  @return The calculated value
 */
typedef float (*single_mat_operator)(float a, float *args);
/**
 * Apply a single_mat_operator to all values of an ip_mat (Change the in ip_mat)
 * @param in The input ip_mat
 * @param op The operation to apply
 * @param args The arguments for the operator
 */
void single_ip_mat_operation_no_copy(ip_mat *in, single_mat_operator op, float *args)
{
    ui iH, iW, iK;

    exit_if_not(in != NULL);

    for(iH = 0; iH < in->h; iH++)
    {
        for(iW = 0; iW < in->w; iW++)
        {
            for(iK = 0; iK < in->k; iK++)
            {
                set_val
                (
                    in,
                    iH,
                    iW,
                    iK,
                    op(get_val(in, iH, iW, iK), args)
                );
            }
        }
    }
    compute_stats(in);
}
/**
 * Apply a single_mat_operator to all values of an ip_mat (Returns a new ip_mat)
 * @param in The input ip_mat
 * @param op The operation to apply
 * @param args The arguments for the operator
 * @return The new ip_mat with the operations applied
 */
ip_mat * single_ip_mat_operation(ip_mat *in, single_mat_operator op, float *args)
{
    /* Can't use copy and single_ip_mat_operation_no_copy because copy use this method */
    ip_mat *out = ip_mat_create(in->h, in->w, in->k, 0.);
    ui iH, iW, iK;

    exit_if_not(in  != NULL);

    for(iH = 0; iH < in->h; iH++)
    {
        for(iW = 0; iW < in->w; iW++)
        {
            for(iK = 0; iK < in->k; iK++)
            {
                set_val
                (
                    out,
                    iH,
                    iW,
                    iK,
                    op(get_val(in, iH, iW, iK), args)
                );
            }
        }
    }
    compute_stats(out);

    return out;
}

/**
 * Use this type to declare an operator that take two value and an array of parameters
 *  @param a The first input value
 *  @param b The second input value
 *  @param args An array of parameters
 *  @return The calculated value
 */
typedef float (*two_mat_operator)(float a, float b, float *args);
/**
 * Apply a two_mat_operator to combine two ip_mat (Returns a new ip_mat)
 * @param a The first input ip_mat
 * @param b The second input ip_mat
 * @param op The operation to apply
 * @param args The arguments for the operator
 * @return The new ip_mat with the operations applied
 */
ip_mat * two_ip_mat_operation(ip_mat *a, ip_mat *b, two_mat_operator op, float *args)
{
    ui iH, iW, iK;
    ui h, w, k;
    ip_mat *out;

    exit_if_not(a != NULL);
    exit_if_not(b != NULL);
    exit_if_not(a->h == b->h);
    exit_if_not(a->w == b->w);
    exit_if_not(a->k == b->k);

    h = a->h;
    w = a->w;
    k = a->k;
    out = ip_mat_create(h, w, k, 0);

    for(iH = 0; iH < h; iH++)
    {
        for(iW = 0; iW < w; iW++)
        {
            for(iK = 0; iK < k; iK++)
            {
                set_val
                (
                    out,
                    iH,
                    iW,
                    iK,
                    op(get_val(a, iH, iW, iK), get_val(b, iH, iW, iK), args)
                );
            }
        }
    }
    compute_stats(out);

    return out;
}

/**
 * Use this type to declare an operator that operate on an array of channels with side effects
 *  @param in The input ip_mat
 *  @param iH Height index
 *  @param iW Width index
 *  @param args An array of parameters
 */
typedef void (*multi_channels_operator)(ip_mat *in, ui iH, ui iW, float *args);
/**
 * Apply a multi_channels_operator to an ip_mat (Change the in ip_mat)
 * @param in The input ip_mat
 * @param op The operation to apply
 * @param args The arguments for the operator
 */
void channels_ip_mat_operation_no_copy(ip_mat * in, multi_channels_operator op, float *args)
{
    ui iH, iW;

    exit_if_not(in != NULL);

    for(iH = 0; iH < in->h; iH++)
    {
        for(iW = 0; iW < in->w; iW++)
        {
            op(in, iH, iW, args); /* Op use side-effect to change data */
        }
    }
    compute_stats(in);
}
/**
 * Apply a multi_channels_operator to an ip_mat (Returns a new ip_mat)
 * @param in The input ip_mat
 * @param op The operation to apply
 * @param args The arguments for the operator
 * @return The new ip_mat with the operations applied
 */
ip_mat *channels_ip_mat_operation(ip_mat *in, multi_channels_operator op, float *args)
{
    ip_mat *out = ip_mat_copy(in);
    channels_ip_mat_operation_no_copy(out, op, args);
    return out;
}

/**** PARTE 1: TIPO DI DATI IP_MAT E MEMORIA ****/

ip_mat *ip_mat_create(unsigned int h, unsigned int w,unsigned  int k, float v)
{
    ui iH, iW, iK;
    ip_mat *newMat = (ip_mat*)malloc(sizeof(ip_mat));
    exit_if_not(newMat != NULL); /* Check if malloc is successfully */
    newMat->h = h;
    newMat->w = w;
    newMat->k = k;

    /* Fill Data */
    if(h > 0) /* It's unsafe to do a malloc of size 0 */
    {
        newMat->data = (float ***) malloc(sizeof(float **) * h);
        exit_if_not(newMat->data != NULL);
        for(iH = 0; iH < h; iH++)
        {
            if(w > 0)
            {
                newMat->data[iH] = (float **) malloc(sizeof(float *) * w);
                exit_if_not(newMat->data[iH] != NULL);
                for(iW = 0; iW < w; iW++)
                {
                    if(k > 0)
                    {
                        newMat->data[iH][iW] = (float *) malloc(sizeof(float) * k);
                        exit_if_not(newMat->data[iH][iW] != NULL);
                        for(iK = 0; iK < k; iK++)
                        {
                            set_val(newMat, iH, iW, iK, v);
                        }
                    }
                }
            }
        }
    }

    /* Fill Stats */
    if(k > 0)
    {
        newMat->stat = (stats *) malloc(sizeof(stats) * k);
        exit_if_not(newMat->stat != NULL);
        for(iK = 0; iK < k; iK++)
        {
            newMat->stat[iK].min = v;
            newMat->stat[iK].max = v;
            newMat->stat[iK].mean = v;
        }
    }

    return newMat;
}

void ip_mat_free(ip_mat *a)
{
    if(a) /* if a is not null */
    {
        if(a->k > 0)
        {
            /* Free Stats */
            free(a->stat);
        }

        /* Free Data */
        if(a->h > 0) /* malloc of size 0 aren't done */
        {
            if(a->w > 0)
            {
                ui iH;
                for(iH = 0; iH < a->h; iH++)
                {
                    if(a->k > 0)
                    {
                        ui iW;
                        for(iW = 0; iW < a->w; iW++)
                        {
                            free(a->data[iH][iW]);
                        }
                    }
                    free(a->data[iH]);
                }
            }
            free(a->data);
        }
        free(a);
    }
}

float get_val(ip_mat *a, unsigned int i, unsigned int j, unsigned int k)
{
    exit_if_not(a != NULL);

    if(i < a->h && j < a->w && k < a->k)
    {
        return a->data[i][j][k];
    }
    else
    {
        printf("get_val: Index Out of Bound");
        exit(1);
    }
}

void set_val(ip_mat *a, unsigned int i, unsigned int j, unsigned int k, float v)
{
    exit_if_not(a != NULL);

    if(i < a->h && j < a->w && k < a->k)
    {
        a->data[i][j][k] = v;
    }
    else
    {
        printf("get_val: Index Out of Bound");
        exit(1);
    }
}

void compute_stats(ip_mat *t)
{
    ui iH, iW, iK;
    float min, max, val;
    float sum;

    exit_if_not(t != NULL);

    for(iK = 0; iK < t->k; iK++)
    {
        min = FLT_MAX;
        max = -FLT_MAX;
        sum = 0;
        for(iH = 0; iH < t->h; iH++)
        {
            for(iW = 0; iW < t->w; iW++)
            {
                val = get_val(t, iH, iW, iK);
                sum += val;
                min = MY_MIN(min, val);
                max = MY_MAX(max, val);
            }
        }
        t->stat[iK].min = min;
        t->stat[iK].max = max;
        t->stat[iK].mean = sum / (t->h * t->w * 1.);
    }
}

float init_random_op(float a, float *args) { UNUSED(a); return get_normal_random(args[0], args[1]); }
void ip_mat_init_random(ip_mat * t, float mean, float std)
{
    float args[2];
    args[0] = mean;
    args[1] = std;
    single_ip_mat_operation_no_copy(t, init_random_op, args);
}

float copy_op(float a, float *args) { UNUSED(args); return a; }
ip_mat *ip_mat_copy(ip_mat *in)
{
    return single_ip_mat_operation(in, copy_op, NULL);
}

ip_mat *ip_mat_subset(ip_mat *t, unsigned int row_start, unsigned int row_end, unsigned int col_start, unsigned int col_end)
{
    ui iH, iW, iK;
    ui h = row_end - row_start;
    ui w = col_end - col_start;
    ip_mat *out;

    exit_if_not(t != NULL);
    exit_if_not(row_end <= t->h);
    exit_if_not(col_end <= t->w);
    exit_if_not(row_end >= row_start);
    exit_if_not(col_end >= col_start);

    out = ip_mat_create(h, w, t->k, 0);

    /* Fill Data */
    for(iH = 0; iH < h; iH++)
    {
        for(iW = 0; iW < w; iW++)
        {
            for(iK = 0; iK < t->k; iK++)
            {
                set_val
                (
                    out,
                    iH,
                    iW,
                    iK,
                    get_val(t, iH + row_start, iW + col_start, iK)
                );
            }
        }
    }

    /* Fill Stats */
    compute_stats(out);

    return out;
}

ip_mat *ip_mat_concat(ip_mat *a, ip_mat *b, int dimensione)
{
    ui iH, iW, iK;
    ui h, w, k;
    ip_mat *out;

    exit_if_not(a != NULL);
    exit_if_not(b != NULL);

    switch(dimensione)
    {
        case 0:
            h = a->h + b->h;
            w = a->w;
            k = a->k;
            exit_if_not(a->w == b->w);
            exit_if_not(a->k == b->k);
            break;
        case 1:
            h = a->h;
            w = a->w + b->w;
            k = a->k;
            exit_if_not(a->h == b->h);
            exit_if_not(a->k == b->k);
            break;
        case 2:
            h = a->h;
            w = a->w;
            k = a->k + b->k;
            exit_if_not(a->h == b->h);
            exit_if_not(a->w == b->w);
            break;
        default:
            printf("ip_mat_concat: dimension %d not supported", dimensione);
            exit(1);
    }

    out = ip_mat_create(h, w, k, 0);

    /* Fill Data */
    for(iH = 0; iH < h; iH++)
    {
        for(iW = 0; iW < w; iW++)
        {
            for(iK = 0; iK < k; iK++)
            {
                float val;

                if(iH < a->h && iW < a->w && iK < a->k)
                {
                    val = get_val(a, iH, iW, iK);
                }
                else
                {
                    switch(dimensione)
                    {
                        case 0:
                            val = get_val(b, iH - a->h, iW, iK);
                            break;
                        case 1:
                            val = get_val(b, iH, iW - a->w, iK);
                            break;
                        case 2:
                            val = get_val(b, iH, iW, iK - a->k);
                            break;
                        default:
                            printf("ip_mat_concat: dimension %d not supported", dimensione);
                            exit(1);
                    }
                }

                set_val(out, iH, iW, iK, val);
            }
        }
    }

    /* Fill Stats */
    compute_stats(out);

    return out;
}

float sum_op(float a, float b, float *args) { UNUSED(args); return a+b; }
ip_mat *ip_mat_sum(ip_mat *a, ip_mat *b)
{
    return two_ip_mat_operation(a, b, sum_op, NULL);
}

float sub_op(float a, float b, float *args) { UNUSED(args); return a-b; }
ip_mat *ip_mat_sub(ip_mat *a, ip_mat *b)
{
    return two_ip_mat_operation(a, b, sub_op, NULL);
}

float mul_op(float a, float *args) { return a*args[0]; }
ip_mat *ip_mat_mul_scalar(ip_mat *a, float c)
{
    float args[1];
    args[0] = c;
    return single_ip_mat_operation(a, mul_op, args);
}

float add_op(float a, float *args) { return a+args[0]; }
ip_mat *ip_mat_add_scalar(ip_mat *a, float c)
{
    float args[1];
    args[0] = c;
    return single_ip_mat_operation(a, add_op, args);
}

/* float mean_op(float a, float b, float _const) { return (a+b)/2.; } */
ip_mat *ip_mat_mean(ip_mat *a, ip_mat *b)
{
    /* return two_ip_mat_operation(a, b, mean_op, NULL); */
    return ip_mat_blend(a, b, 0.5);
}

/**** PARTE 2: SEMPLICI OPERAZIONI SU IMMAGINI ****/
void grey_scale_op(ip_mat *in, ui iH, ui iW, float *args)
{
    ui iK;
    float sum = 0;
    float avg;

    UNUSED(args);

    for(iK = 0; iK < in->k; iK++)
    {
        sum += get_val(in, iH, iW, iK);
    }
    avg = sum / (in->k * 1.);
    for(iK = 0; iK < in->k; iK++)
    {
        set_val(in, iH, iW, iK, avg);
    }
}
ip_mat *ip_mat_to_gray_scale(ip_mat *in)
{
    return channels_ip_mat_operation(in, grey_scale_op, NULL);
}

float blend_op(float a, float b, float *args) { return (a * args[0]) + (b * (1-args[0])); }
ip_mat *ip_mat_blend(ip_mat *a, ip_mat *b, float alpha)
{
    float args[1];
    args[0] = alpha;
    exit_if_not(0 <= alpha && alpha <= 1); /* Alpha must be between [0, 1] */
    return two_ip_mat_operation(a, b, blend_op, args);
}

ip_mat *ip_mat_brighten(ip_mat *a, float bright)
{
    return ip_mat_add_scalar(a, bright);
}

float corrupt_op(float a, float *args) { return a + get_normal_random(0, args[0]); }
ip_mat *ip_mat_corrupt(ip_mat * a, float amount)
{
    float args[1];
    args[0] = amount / 2.;
    return single_ip_mat_operation(a, corrupt_op, args);
    /* return ip_mat_add_scalar(a, get_normal_random() * amount); */
}

/**** PART 3 PRIVATE FUNCTIONS ****/
/**
 * Take a linear array and return an ip_mat
 * @param in A linear float array with size (w*h)
 * @param w The width of the filter
 * @param h The height of the filter
 * @return The newly created ip_mat
 */
ip_mat *create_filter_from_linear_array(float *in, ui w, ui h)
{
    ui iW, iH;
    ip_mat *out = ip_mat_create(h, w, 1, 0.);

    for(iH = 0; iH < h; iH++)
    {
        for(iW = 0; iW < w; iW++)
        {
            set_val(out, iH, iW, 0, in[(iH * w) + iW]);
        }
    }
    compute_stats(out);

    return out;
}

/**
 * Multiply a mat for a filter in one position
 * @param mat The input matrix
 * @param filter The filter matrix
 * @param posH The height position
 * @param posW The height position
 * @param matChannel The channel of the matrix
 * @param filterChannel The channel of the filter
 * @return The calculated value
 */
float ip_mat_mul_filter(ip_mat *mat, ip_mat *filter, ui posH, ui posW, ui matChannel, ui filterChannel)
{
    float out = 0;
    ui iH, iW;
    exit_if_not(mat->h >= posH + filter->h);
    exit_if_not(mat->w >= posW + filter->w);
    exit_if_not(mat->k > matChannel);
    exit_if_not(filter->k > filterChannel);

    for(iH = 0; iH < filter->h; iH++)
    {
        for(iW = 0; iW < filter->w; iW++)
        {
            out += get_val(mat, posH+iH, posW+iW, matChannel) * get_val(filter, iH, iW, filterChannel);
        }
    }

    return out;
}

/**** PARTE 3: CONVOLUZIONE E FILTRI *****/
ip_mat *ip_mat_convolve(ip_mat *a, ip_mat *f)
{
    ip_mat *out, *paddedIn;
    ui iH, iW, iK;
    ui padH, padW;

    exit_if_not(a != NULL);
    exit_if_not(f != NULL);

    if(SINGLE_CHANNEL_CONVOLVE)
        exit_if_not(f->k == 1 || f->k == a->k); /* Same filter for all channel or one filter per channel */
    else
        exit_if_not(f->k == a->k); /* One filter per channel */

    padH = (f->h - 1) / 2; /* The integer division is intended */
    padW = (f->w - 1) / 2;
    paddedIn = ip_mat_padding(a, padH, padW);
    out = ip_mat_create(a->h, a->w, a->k, 0.);

    for(iH = 0; iH < out->h; iH++)
    {
        for(iW = 0; iW < out->w; iW++)
        {
            for(iK = 0; iK < out->k; iK++)
            {
                ui filterChannel = 0;
                if(f->k != 1) filterChannel = iK;

                set_val
                (
                    out,
                    iH,
                    iW,
                    iK,
                    ip_mat_mul_filter(paddedIn, f, iH, iW, iK, filterChannel)
                );
            }
        }
    }

    ip_mat_free(paddedIn);
    compute_stats(out);
    return out;
}

ip_mat *ip_mat_padding(ip_mat * a, unsigned int pad_h, unsigned int pad_w)
{
    ip_mat *horizontal, *vertical, *temp, *out;

    /* Top - Bottom */
    horizontal = ip_mat_create(pad_h, a->w, a->k, 0.);

    /* Left - Right */
    vertical = ip_mat_create(a->h + (2 * pad_h), pad_w, a->k, 0.);

    /* Top */
    temp = a;
    out = ip_mat_concat(horizontal, temp, 0);
    /* Do not free a */

    /* Bottom */
    temp = out;
    out = ip_mat_concat(temp, horizontal, 0);
    ip_mat_free(temp);

    /* Left */
    temp = out;
    out = ip_mat_concat(vertical, temp, 1);
    ip_mat_free(temp);

    /* Right */
    temp = out;
    out = ip_mat_concat(temp, vertical, 1);
    ip_mat_free(temp);

    ip_mat_free(horizontal);
    ip_mat_free(vertical);

    /* Stats is calculated by concat  */
    return out;
}

ip_mat *create_sharpen_filter()
{
    float filter[9] =
    {
         0., -1.,  0.,
        -1.,  5., -1.,
         0., -1.,  0.
    };

    return create_filter_from_linear_array(filter, 3, 3);
}

ip_mat *create_edge_filter()
{
    float filter[9] =
    {
        -1., -1.,  -1.,
        -1.,  8.,  -1.,
        -1., -1.,  -1.
    };

    return create_filter_from_linear_array(filter, 3, 3);
}

ip_mat *create_emboss_filter()
{
    float filter[9] =
    {
        -2., -1.,  0.,
        -1.,  1.,  1.,
         0.,  1.,  2.
    };

    return create_filter_from_linear_array(filter, 3, 3);
}

ip_mat *create_average_filter(unsigned int h, unsigned int w, unsigned int k)
{
    float val;
    /* If w or h is 0 then val become Inf */
    exit_if_not(w > 0);
    exit_if_not(h > 0);

    val = 1. / (w * h * 1.);

    /* Create also fill stats */
    return ip_mat_create(h, w, k, val);
}

ip_mat *create_gaussian_filter(unsigned int h, unsigned int w, unsigned int k, float sigma)
{
    ui iH, iW, iK;
    ip_mat *out, *temp;
    int ch, cw; /* Center cell */
    float sum = 0;

    /* Pre-calculate some value that doesn't depend on position */
    float oneDividedByTwoSigmaSqr = 1. / (2. * sigma * sigma);
    float oneDividedByTwoPiSigmaSqr = oneDividedByTwoSigmaSqr * 1. / (PI);

    exit_if_not(w > 0);
    exit_if_not(h > 0);
    exit_if_not(k > 0);
    exit_if_not(w % 2 != 0); /* Must be odd */
    exit_if_not(h % 2 != 0); /* Must be odd */

    ch = (h - 1) / 2; /* integer division is needed */
    cw = (w - 1) / 2;

    temp = ip_mat_create(h, w, k, 0.);

    for(iH = 0; iH < h; iH++)
    {
        for(iW = 0; iW < w; iW++)
        {
            float posH = (int)iH - ch;
            float posW = (int)iW - cw;
            float val = oneDividedByTwoPiSigmaSqr * exp(-1. * (posH*posH + posW*posW) * oneDividedByTwoSigmaSqr);
            sum += val;
            for(iK = 0; iK < k; iK++)
            {
                set_val(temp, iH, iW, iK, val);
            }
        }
    }

    out = ip_mat_mul_scalar(temp, 1. / sum);
    ip_mat_free(temp);

    /* ip_mat_mul also fill stats */
    return out;
}

void rescale_op(ip_mat *in, ui iH, ui iW, float *args)
{
    ui iK;
    for(iK = 0; iK < in->k; iK++)
    {
        float val = (get_val(in, iH, iW, iK) - in->stat[iK].min) / (in->stat[iK].max - in->stat[iK].min);
        val *= args[0];
        set_val(in, iH, iW, iK, val);
    }
}
void rescale(ip_mat *t, float new_max)
{
    float args[1];
    args[0] = new_max;
    compute_stats(t);
    channels_ip_mat_operation_no_copy(t, rescale_op, args);
}

float clamp_op(float a, float *args) { return MY_MAX(args[0], MY_MIN(args[1], a)); }
void clamp(ip_mat *t, float low, float high)
{
    float args[2];
    exit_if_not(low <= high);
    args[0] = low;
    args[1] = high;
    single_ip_mat_operation_no_copy(t, clamp_op, args);
}

/**** METODI GIA' IMPLEMENTATI ****/

ip_mat *bitmap_to_ip_mat(Bitmap *img)
{
    unsigned int i = 0, j = 0;

    unsigned char R, G, B;

    unsigned int h = img->h;
    unsigned int w = img->w;

    ip_mat *out = ip_mat_create(h, w, 3, 0);

    for(i = 0; i < h; i++)              /* rows */
    {
        for(j = 0; j < w; j++)          /* columns */
        {
            bm_get_pixel(img, j, i, &R, &G, &B);
            set_val(out, i, j, 0, (float) R);
            set_val(out, i, j, 1, (float) G);
            set_val(out, i, j, 2, (float) B);
        }
    }

    compute_stats(out);

    return out;
}

Bitmap *ip_mat_to_bitmap(ip_mat *t)
{
    Bitmap *b = bm_create(t->w, t->h);

    unsigned int i, j;
    for(i = 0; i < t->h; i++)              /* rows */
    {
        for(j = 0; j < t->w; j++)          /* columns */
        {
            bm_set_pixel(b, j,i, (unsigned char) get_val(t,i,j,0),
                 (unsigned char) get_val(t,i,j,1),
                 (unsigned char) get_val(t,i,j,2));
        }
    }
    return b;
}

void ip_mat_show(ip_mat *t)
{
    unsigned int i, l, j;
    printf("Matrix of size %d * %d * %d (h * w * k)\n", t->h, t->w, t->k);
    for(l = 0; l < t->k; l++)
    {
        printf("Slice %d\n", l);
        for(i = 0; i < t->h; i++)
        {
            for(j = 0; j < t->w; j++)
            {
                printf("%f ", get_val(t, i, j, l));
            }
            printf("\n");
        }
        printf("\n");
    }
}

void ip_mat_show_stats(ip_mat *t)
{
    unsigned int k;

    compute_stats(t);

    for(k = 0; k < t->k; k++)
    {
        printf("Channel %d:\n", k);
        printf("\t Min: %f\n", t->stat[k].min);
        printf("\t Max: %f\n", t->stat[k].max);
        printf("\t Mean: %f\n", t->stat[k].mean);
    }
}

float get_normal_random(float media, float std)
{
    float y1 = ((float) (rand()) + 1.) / ((float) (RAND_MAX) + 1.);
    float y2 = ((float) (rand()) + 1.) / ((float) (RAND_MAX) + 1.);
    float num = cos(2 * PI * y2) * sqrt(-2. * log(y1));

    return media + num * std;
}

/*
My random function made before the 1.1.1 version
float get_normal_random_with_mean_var(float mean, float var)
{
    float sigma;
    exit_if_not(var >= 0);
    sigma = sqrt(var);
    return get_normal_random() * sigma + mean;
}
*/