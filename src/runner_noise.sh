#!/bin/bash
var1="_"
var2=".bmp"
fn="../images/flower2"
method="corrupt"

for amount in 5 10 50 100 255
do
    out=$fn$var1$method$var1$amount
    echo "$out"
    ./main_iplib.out $fn$var2 0 $method $out 0 $amount
done