#include "ip_lib.h"
#include <math.h>
#include <stdio.h>

float get_normal_random2(float mean, float var)
{
    /* Mean=0 Var=1 */
    float sigma = sqrt(var);
    return get_normal_random() * sigma + mean;
}

void try_rand2()
{
    float sum = 0;
    float sumVar = 0;
    float l = 10000000;
    float mean = 1;
    float var = 1000;
    int i;
    float v;
    for(i = 0; i < l; i++)
    {
        v = get_normal_random2(mean, var);
        sum += v;
        sumVar += (v-mean)*(v-mean);
    }

    printf("Mean %f \n", sum/l);
    printf("Var %f", sumVar/l);
}

void my_random()
{
    Bitmap *bm;
    ip_mat *mat = ip_mat_create(200, 200, 3, 0.);
    ip_mat_init_random(mat, 0, 1);
    ip_mat_show_stats(mat);
    rescale(mat, 255);
    ip_mat_show_stats(mat);
    bm = ip_mat_to_bitmap(mat);
    bm_save(bm, "../output/random.bmp");

    bm_free(bm);
    ip_mat_free(mat);
}

void test_subset()
{
    ip_mat *a, *b, *c;
    a = ip_mat_create(10, 3, 2, 0);
    ip_mat_show(a);
    ip_mat_init_random(a, 100, 100);
    ip_mat_show(a);
    b = ip_mat_copy(a);
    ip_mat_show(b);
    c = ip_mat_subset(b, 7, 10, 1, 3);
    ip_mat_show(c);

    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(c);
}

void crop()
{
    Bitmap *bm = bm_load("../images/mandelbrot.bmp");
    ip_mat *mat = bitmap_to_ip_mat(bm);
    ip_mat *mat_cropped = ip_mat_subset(mat, 0, mat->h/2, 0, mat->w);
    Bitmap *bm_cropped = ip_mat_to_bitmap(mat_cropped);
    bm_save(bm_cropped, "../output/half_mandelbrot.bmp");

    bm_free(bm);
    bm_free(bm_cropped);
    ip_mat_free(mat);
    ip_mat_free(mat_cropped);
}

void greyscale()
{
    Bitmap *bm = bm_load("../images/flower.bmp");
    ip_mat *mat = bitmap_to_ip_mat(bm);
    ip_mat *mat_grey = ip_mat_to_gray_scale(mat);
    Bitmap *bm_grey = ip_mat_to_bitmap(mat_grey);
    bm_save(bm_grey, "../output/grey_flower.bmp");

    bm_free(bm);
    bm_free(bm_grey);
    ip_mat_free(mat);
    ip_mat_free(mat_grey);
}

void blending()
{
    Bitmap *bm_a = bm_load("../images/mongolfiere.bmp");
    Bitmap *bm_b = bm_load("../images/flower2.bmp");
    ip_mat *mat_a = bitmap_to_ip_mat(bm_a);
    ip_mat *mat_b = bitmap_to_ip_mat(bm_b);

    ip_mat *mat_r1 = ip_mat_blend(mat_a, mat_b, 0.);
    ip_mat *mat_r2 = ip_mat_blend(mat_a, mat_b, 0.25);
    ip_mat *mat_r3 = ip_mat_blend(mat_a, mat_b, 0.5);
    ip_mat *mat_r4 = ip_mat_blend(mat_a, mat_b, 0.75);
    ip_mat *mat_r5 = ip_mat_blend(mat_a, mat_b, 1.);

    Bitmap *bm_r1 = ip_mat_to_bitmap(mat_r1);
    Bitmap *bm_r2 = ip_mat_to_bitmap(mat_r2);
    Bitmap *bm_r3 = ip_mat_to_bitmap(mat_r3);
    Bitmap *bm_r4 = ip_mat_to_bitmap(mat_r4);
    Bitmap *bm_r5 = ip_mat_to_bitmap(mat_r5);
    bm_save(bm_r1, "../output/blend0_00.bmp");
    bm_save(bm_r2, "../output/blend0_25.bmp");
    bm_save(bm_r3, "../output/blend0_50.bmp");
    bm_save(bm_r4, "../output/blend0_75.bmp");
    bm_save(bm_r5, "../output/blend1_00.bmp");

    bm_free(bm_r1);
    bm_free(bm_r2);
    bm_free(bm_r3);
    bm_free(bm_r4);
    bm_free(bm_r5);

    ip_mat_free(mat_r1);
    ip_mat_free(mat_r2);
    ip_mat_free(mat_r3);
    ip_mat_free(mat_r4);
    ip_mat_free(mat_r5);

    ip_mat_free(mat_a);
    ip_mat_free(mat_b);
    bm_free(bm_a);
    bm_free(bm_b);
}

void corrupt()
{
    Bitmap *bm_r1, *bm_r2, *bm_r3, *bm_r4, *bm_r5, *bm_r6;
    Bitmap *bm = bm_load("../images/flower2.bmp");
    ip_mat *mat = bitmap_to_ip_mat(bm);

    ip_mat *mat_r1 = ip_mat_corrupt(mat, 0.);
    ip_mat *mat_r2 = ip_mat_corrupt(mat, 5.);
    ip_mat *mat_r3 = ip_mat_corrupt(mat, 10.);
    ip_mat *mat_r4 = ip_mat_corrupt(mat, 50.);
    ip_mat *mat_r5 = ip_mat_corrupt(mat, 100.);
    ip_mat *mat_r6 = ip_mat_corrupt(mat, 255.);

    clamp(mat_r1, 0, 255);
    clamp(mat_r2, 0, 255);
    clamp(mat_r3, 0, 255);
    clamp(mat_r4, 0, 255);
    clamp(mat_r5, 0, 255);
    clamp(mat_r6, 0, 255);

    bm_r1 = ip_mat_to_bitmap(mat_r1);
    bm_r2 = ip_mat_to_bitmap(mat_r2);
    bm_r3 = ip_mat_to_bitmap(mat_r3);
    bm_r4 = ip_mat_to_bitmap(mat_r4);
    bm_r5 = ip_mat_to_bitmap(mat_r5);
    bm_r6 = ip_mat_to_bitmap(mat_r6);
    bm_save(bm_r1, "../output/corrupt000.bmp");
    bm_save(bm_r2, "../output/corrupt005.bmp");
    bm_save(bm_r3, "../output/corrupt010.bmp");
    bm_save(bm_r4, "../output/corrupt050.bmp");
    bm_save(bm_r5, "../output/corrupt100.bmp");
    bm_save(bm_r6, "../output/corrupt255.bmp");

    bm_free(bm_r1);
    bm_free(bm_r2);
    bm_free(bm_r3);
    bm_free(bm_r4);
    bm_free(bm_r5);
    bm_free(bm_r6);

    ip_mat_free(mat_r1);
    ip_mat_free(mat_r2);
    ip_mat_free(mat_r3);
    ip_mat_free(mat_r4);
    ip_mat_free(mat_r5);
    ip_mat_free(mat_r6);

    ip_mat_free(mat);
    bm_free(bm);
}

void bright()
{
    Bitmap *bm_r1, *bm_r2, *bm_r3, *bm_r4, *bm_r5, *bm_r6;
    Bitmap *bm = bm_load("../images/flower2.bmp");
    ip_mat *mat = bitmap_to_ip_mat(bm);

    ip_mat *mat_r1 = ip_mat_brighten(mat, 0.);
    ip_mat *mat_r2 = ip_mat_brighten(mat, 5.);
    ip_mat *mat_r3 = ip_mat_brighten(mat, 10.);
    ip_mat *mat_r4 = ip_mat_brighten(mat, 50.);
    ip_mat *mat_r5 = ip_mat_brighten(mat, 100.);
    ip_mat *mat_r6 = ip_mat_brighten(mat, 255.);

    clamp(mat_r1, 0, 255);
    clamp(mat_r2, 0, 255);
    clamp(mat_r3, 0, 255);
    clamp(mat_r4, 0, 255);
    clamp(mat_r5, 0, 255);
    clamp(mat_r6, 0, 255);

    bm_r1 = ip_mat_to_bitmap(mat_r1);
    bm_r2 = ip_mat_to_bitmap(mat_r2);
    bm_r3 = ip_mat_to_bitmap(mat_r3);
    bm_r4 = ip_mat_to_bitmap(mat_r4);
    bm_r5 = ip_mat_to_bitmap(mat_r5);
    bm_r6 = ip_mat_to_bitmap(mat_r6);
    bm_save(bm_r1, "../output/bright000.bmp");
    bm_save(bm_r2, "../output/bright005.bmp");
    bm_save(bm_r3, "../output/bright010.bmp");
    bm_save(bm_r4, "../output/bright050.bmp");
    bm_save(bm_r5, "../output/bright100.bmp");
    bm_save(bm_r6, "../output/bright255.bmp");

    bm_free(bm_r1);
    bm_free(bm_r2);
    bm_free(bm_r3);
    bm_free(bm_r4);
    bm_free(bm_r5);
    bm_free(bm_r6);

    ip_mat_free(mat_r1);
    ip_mat_free(mat_r2);
    ip_mat_free(mat_r3);
    ip_mat_free(mat_r4);
    ip_mat_free(mat_r5);
    ip_mat_free(mat_r6);

    ip_mat_free(mat);
    bm_free(bm);
}

void concat()
{
    ip_mat *a, *c, *o;
    a = ip_mat_create(10, 3, 1, 0);
    ip_mat_init_random(a, 100, 100);
    ip_mat_show(a);

    c = ip_mat_create(10, 3, 2, 254);
    ip_mat_show(c);

    o = ip_mat_concat(a, c, 2);
    ip_mat_show(o);

    ip_mat_free(a);
    ip_mat_free(c);
    ip_mat_free(o);
}

void pad()
{
    ip_mat *a, *b;
    a = ip_mat_create(5, 3, 1, 0);
    ip_mat_init_random(a, 100, 100);
    ip_mat_show(a);
    b = ip_mat_padding(a, 0, 0);
    ip_mat_show(b);

    ip_mat_free(a);
    ip_mat_free(b);
}

void get_gauss()
{
    ip_mat *a, *b, *f;

    a = ip_mat_create(5, 3, 1, 10);
    ip_mat_init_random(a, 100, 100);

    f = create_gaussian_filter(9, 9, 1, 0.1);

    b = ip_mat_convolve(a, f);

    ip_mat_show(a);
    ip_mat_show(b);

    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(f);
}

void test_mem()
{
    ip_mat *a;
    a = ip_mat_create(5, 3, 1, 0);

    ip_mat_free(a);
}

void test_convolve()
{
    ip_mat *a, *b, *f;
    a = ip_mat_create(5, 3, 1, 0);
    ip_mat_init_random(a, 100, 100);

    f = ip_mat_create(3, 3, 1, 0.);
    set_val(f, 1, 1, 0, 1.);

    b = ip_mat_convolve(a, f);
    ip_mat_show(f);
    ip_mat_show(a);
    ip_mat_show(b);

    ip_mat_free(a);
    ip_mat_free(b);
    ip_mat_free(f);
}


int main()
{
    test_mem();

    test_convolve();

    /* pad(); */

    my_random();
    crop();
    greyscale();
    blending();
    corrupt();
    bright();

    get_gauss();
    return 0;
}
